leaseplan-back
======================
### API server: Spotify API
```shell
    https://developer.spotify.com/documentation/web-api/reference/
```

# Oauth
```shell
    https://accounts.spotify.com/api/token
```

# Get an Artist's Related Artists
```shell
	https://api.spotify.com/v1/artists/{id}/related-artists
```

# Get an Artist's Top Tracks
```shell
    https://api.spotify.com/v1/artists/{id}/top-tracks
```
### Tests info

I'm recovering the information from an artist's related artists and then getting their top tracks. I'm also testing that I get an error when I use a wrong artist id.

### Tech stack used:
- Java 1.8
- Serenity
- RestAssured

### Installing and running the project:

- Download or Clone the project.
- Modify the configuration file to match your own client_id and client_secret.
- To run the tests from terminal:

```shell
mvn -B verify --file pom.xml
```

### Test reports

- You can find the test reports in the test-results folder, including the cucumber and serenity formats.
- If you use Gitlab you can download the results as artifacts after the execution of the job.
