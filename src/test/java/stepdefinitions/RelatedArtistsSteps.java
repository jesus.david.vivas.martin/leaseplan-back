package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;
import utilities.ApiUtils;

public class RelatedArtistsSteps {
	private Response relatedArtistsResponse;
	private Response topTracksResponse;
	private String endpoint;
	
	@Given("an endpoint: {string}")
	public void and_endpoint_string(String endpoint) {
		this.endpoint = endpoint;
	}

	@When("I call the endpoint with get") 
	public void i_call_the_endpoint_with_get() {
		relatedArtistsResponse = ApiUtils.get(endpoint);
	}
	
	@Then("I get a response status: {int}")
	public void i_get_a_response_status_int(int status) {
		Assert.assertEquals(status, relatedArtistsResponse.statusCode());
	}
	
	@Then("I can get the top tracks from the related artist")
	public void i_can_get_the_top_tracks_from_the_related_artist() {
		for (int i = 0;i < relatedArtistsResponse.jsonPath().getList("artists").size(); i++) {
			String topTracksEndpoint = 	"https://api.spotify.com/v1/artists/" + relatedArtistsResponse.jsonPath().getString("artists["+i+"].id")+"/top-tracks?market=ES";
			topTracksResponse = ApiUtils.get(topTracksEndpoint);
			Assert.assertEquals(200, topTracksResponse.getStatusCode());
		}
		
	}
}
