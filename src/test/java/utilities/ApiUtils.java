package utilities;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class ApiUtils {

    private final static String base_uri = ConfigReader.getProperty("base_uri");
    private static Response response;

    private static RequestSpecification getRequest() {

        RequestSpecification request = given()
                .baseUri(base_uri)
                .headers(
                        "Accept","application/json",
                        "Content-Type", "application/json",
                        "Authorization", getToken()
                )
                .when();
        return request;
    }

    private static String getToken(){
        Map<String,String> reqBody = new HashMap<>();
        reqBody.put("grant_type", "client_credentials");

        response =
                given()
                	.auth()
                	.preemptive()
                	.basic(ConfigReader.getProperty("client_id"), ConfigReader.getProperty("client_secret"))
                    .contentType("application/x-www-form-urlencoded")
                    .formParams(reqBody)
                    .when()
                    .post("https://accounts.spotify.com/api/token");

        Map<String, Object> map = response.as(HashMap.class);
        String token = map.get("access_token").toString();

        return "Bearer "+token;
    }

    public static Response post(String pathParams, Object reqBody) {
        try {
            response = getRequest().body(reqBody).post(pathParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static Response get(String pathParams) {
        try {
            response = getRequest().get(pathParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static Response put(String pathParams, Object reqBody) {
        try {
            response = getRequest().body(reqBody).put(pathParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static Response delete(String pathParams) {
        try {
            response = getRequest().put(pathParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
