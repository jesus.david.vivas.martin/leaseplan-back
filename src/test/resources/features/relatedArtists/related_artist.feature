@smoke @relatedArtists
Feature: Related Artists

  Scenario Outline: Related artists
    Given an endpoint: "artists/<artist_id>/related-artists"
    When I call the endpoint with get
    Then I get a response status: <status>

    Examples:
      | artist_id                | status |
      | 3YQKmKGau1PzlVlkL1iodx   | 200    |
      | 2yEwvVSSSUkcLeSTNyHKh8 	 | 200    |
      | madeupid			 	 | 400    |

  Scenario Outline: Related artists top tracks
  	Given an endpoint: "artists/<artist_id>/related-artists"
  	When I call the endpoint with get
  	Then I can get the top tracks from the related artist
  	
  	Examples:
      | artist_id                | status |
      | 3YQKmKGau1PzlVlkL1iodx   | 200    |
      | 2yEwvVSSSUkcLeSTNyHKh8 	 | 200    |